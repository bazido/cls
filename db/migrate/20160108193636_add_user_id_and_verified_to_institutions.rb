class AddUserIdAndVerifiedToInstitutions < ActiveRecord::Migration
  def change
    add_column :institutions, :user_id, :boolean
  end
end
