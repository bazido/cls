class CreatePastPapers < ActiveRecord::Migration
  def change
    create_table :past_papers do |t|
      t.string :producer
      t.string :paper
      t.string :province
      t.string :month
      t.integer :year
      t.string :subject_type_id

      t.timestamps null: false
    end
    add_index :past_papers, [:subject_type_id, :province, :year]
  end
end
