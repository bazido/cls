class CreateAnnouncements < ActiveRecord::Migration
  def change
    create_table :announcements do |t|
      t.integer :subject_id
      t.integer :user_id
      t.string :title
      t.text :body

      t.timestamps null: false
    end
    add_index :announcements, [:subject_id, :updated_at]
  end
end
