class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.integer :institution_id
      t.integer :user_id
      t.string :name
      t.integer :year
      t.string :subject_type
      t.string :other_subject_type

      t.timestamps null: false
    end
    add_index :subjects, [:institution_id, :subject_type, :name]
  end
end
