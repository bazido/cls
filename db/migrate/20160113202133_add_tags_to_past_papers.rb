class AddTagsToPastPapers < ActiveRecord::Migration
  def change
    add_column :past_papers, :tags, :string
  end
end
