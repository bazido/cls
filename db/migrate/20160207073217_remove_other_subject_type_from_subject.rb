class RemoveOtherSubjectTypeFromSubject < ActiveRecord::Migration
  def change
    remove_column :subjects, :other_subject_type
  end
end
