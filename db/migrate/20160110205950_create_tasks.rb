class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.integer :subject_id, null: false
      t.integer :user_id, null: false, index: true
      t.integer :task_type_id, null: false
      t.string :lifetime, null: false, default: 'unlimited'
      t.datetime :start_at
      t.datetime :end_at
      t.string :is_public, default: false
      t.string :title, null: false
      t.text :body, null: false

      t.timestamps null: false
    end
    add_index :tasks, [:subject_id, :start_at, :end_at, :is_public]
  end
end
