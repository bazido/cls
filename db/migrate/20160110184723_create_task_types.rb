class CreateTaskTypes < ActiveRecord::Migration
  def change
    create_table :task_types do |t|
      t.integer :user_id, null: false
      t.string :name, null: false
      t.integer :subject_id, null: false

      t.timestamps null: false
    end
    add_index :task_types, [:subject_id, :name]
  end
end
