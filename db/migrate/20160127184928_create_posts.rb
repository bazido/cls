class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :subject_id, null: false
      t.integer :user_id, index: true
      t.string :title, null: false, default: ''
      t.text :body, null: false, default: ''
      t.boolean :open, null: false, default: true
      t.string :post_type, null: false

      t.timestamps null: false
    end
    add_index :posts, [:post_type, :subject_id, :user_id]
  end
end
