class RemoveSubjectTypeIdFromPastPaper < ActiveRecord::Migration
  def change
    remove_column :past_papers, :subject_type_id
  end
end
