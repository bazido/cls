class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.integer :subject_id, null: false
      t.integer :user_id, null: false
      t.boolean :accepted, null: false, default: false

      t.timestamps null: false
    end
    add_index :students, [:user_id, :subject_id, :accepted]
  end
end
