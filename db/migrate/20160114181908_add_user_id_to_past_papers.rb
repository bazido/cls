class AddUserIdToPastPapers < ActiveRecord::Migration
  def change
    add_column :past_papers, :user_id, :integer
  end
end
