class CreateInstitutions < ActiveRecord::Migration
  def change
    create_table :institutions do |t|
      t.string :location
      t.string :name
      t.string :website
      t.text :bio
      t.string :institution_type

      t.timestamps null: false
    end
    add_index :institutions, [:name, :location]
    add_index :institutions, :website
  end
end
