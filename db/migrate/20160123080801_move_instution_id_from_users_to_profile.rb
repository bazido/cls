class MoveInstutionIdFromUsersToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :institution_id, :integer, index: true
    execute "UPDATE profiles SET institution_id=u.institution_id FROM users u WHERE u.id=user_id"
    remove_column :users, :institution_id
  end
end
