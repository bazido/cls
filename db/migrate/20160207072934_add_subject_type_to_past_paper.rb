class AddSubjectTypeToPastPaper < ActiveRecord::Migration
  def change
    add_column :past_papers, :subject_type, :string, index: true, default: ''
  end
end
