class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id, index: true
      t.string :fullname
      t.text :bio
      t.integer :year
      t.boolean :is_public

      t.timestamps null: false
    end
    add_index :profiles, [:year, :is_public]
    add_index :profiles, :fullname
  end
end
