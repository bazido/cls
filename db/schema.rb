# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160207073217) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "announcements", force: :cascade do |t|
    t.integer  "subject_id"
    t.integer  "user_id"
    t.string   "title"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "announcements", ["subject_id", "updated_at"], name: "index_announcements_on_subject_id_and_updated_at", using: :btree

  create_table "attachments", force: :cascade do |t|
    t.string   "attachable_type"
    t.integer  "attachable_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.text     "body"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "images", force: :cascade do |t|
    t.string   "imageable_type"
    t.integer  "imageable_id"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "display_file_name"
    t.string   "display_content_type"
    t.integer  "display_file_size"
    t.datetime "display_updated_at"
  end

  create_table "institutions", force: :cascade do |t|
    t.string   "location"
    t.string   "name"
    t.string   "website"
    t.text     "bio"
    t.string   "institution_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.boolean  "user_id"
  end

  add_index "institutions", ["name", "location"], name: "index_institutions_on_name_and_location", using: :btree
  add_index "institutions", ["website"], name: "index_institutions_on_website", using: :btree

  create_table "past_papers", force: :cascade do |t|
    t.string   "producer"
    t.string   "paper"
    t.string   "province"
    t.string   "month"
    t.integer  "year"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "tags"
    t.integer  "user_id"
    t.string   "subject_type", default: ""
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "subject_id",                null: false
    t.integer  "user_id"
    t.string   "title",      default: "",   null: false
    t.text     "body",       default: "",   null: false
    t.boolean  "open",       default: true, null: false
    t.string   "post_type",                 null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "posts", ["post_type", "subject_id", "user_id"], name: "index_posts_on_post_type_and_subject_id_and_user_id", using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "fullname"
    t.text     "bio"
    t.integer  "year"
    t.boolean  "is_public"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "institution_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "profiles", ["fullname"], name: "index_profiles_on_fullname", using: :btree
  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree
  add_index "profiles", ["year", "is_public"], name: "index_profiles_on_year_and_is_public", using: :btree

  create_table "students", force: :cascade do |t|
    t.integer  "subject_id",                 null: false
    t.integer  "user_id",                    null: false
    t.boolean  "accepted",   default: false, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "students", ["user_id", "subject_id", "accepted"], name: "index_students_on_user_id_and_subject_id_and_accepted", using: :btree

  create_table "sub_posts", force: :cascade do |t|
    t.integer  "post_id",    null: false
    t.integer  "user_id",    null: false
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "sub_posts", ["post_id", "user_id"], name: "index_sub_posts_on_post_id_and_user_id", using: :btree

  create_table "subjects", force: :cascade do |t|
    t.integer  "institution_id"
    t.integer  "user_id"
    t.string   "name"
    t.integer  "year"
    t.string   "subject_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "subjects", ["institution_id", "subject_type", "name"], name: "index_subjects_on_institution_id_and_subject_type_and_name", using: :btree

  create_table "task_types", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.string   "name",       null: false
    t.integer  "subject_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "task_types", ["subject_id", "name"], name: "index_task_types_on_subject_id_and_name", using: :btree

  create_table "tasks", force: :cascade do |t|
    t.integer  "subject_id",                         null: false
    t.integer  "user_id",                            null: false
    t.integer  "task_type_id",                       null: false
    t.string   "lifetime",     default: "unlimited", null: false
    t.datetime "start_at"
    t.datetime "end_at"
    t.string   "is_public",    default: "f"
    t.string   "title",                              null: false
    t.text     "body",                               null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "tasks", ["subject_id", "start_at", "end_at", "is_public"], name: "index_tasks_on_subject_id_and_start_at_and_end_at_and_is_public", using: :btree
  add_index "tasks", ["user_id"], name: "index_tasks_on_user_id", using: :btree

  create_table "teachers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "teachers", ["subject_id", "user_id"], name: "index_teachers_on_subject_id_and_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username"
    t.string   "email"
    t.string   "mobile_number"
    t.string   "crypted_password"
    t.string   "role",                default: "student"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.string   "single_access_token"
    t.string   "perishable_token"
    t.integer  "login_count",         default: 0,         null: false
    t.integer  "failed_login_count",  default: 0,         null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.boolean  "confirmed",           default: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "validated",           default: false,     null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["mobile_number"], name: "index_users_on_mobile_number", using: :btree
  add_index "users", ["username"], name: "index_users_on_username", using: :btree

end
