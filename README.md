# About

This is just a small CMS if you want to call it that for a class room
kind of like BlackBoard but using Rails.

## Caveat
The are no tests. :grin: Sorry... The next project will have them :smile:

## Note
This is the reason I abandoned my two other projects. The other reason is
that I used VueJS for a bit and started questioning myself on whether that
application needs react or VueJS would do just fine. VueJS is easy to grasp
and I seemed very productive.

## Sneak peak

I have the system up on http://www.bazido.com:7000 as I develop
