Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'users#login'
  resources :users do
    member do
      post 'reset-password' => 'users#reset_password'
    end
    collection do
      get 'login' => 'users#login'
      get 'logout' => 'users#logout'
      post 'sessions' => 'users#sessions'
    end
  end

  resources :institutions do
    member do
      get 'list' => 'institutions#list', path: 'list-subjects'
      get 'add' => 'subjects#add', path: 'add-subject'
      get 'remove' => 'subjects#remove', path: 'remove-subject'

      get 'join' => 'institutions#join'
      get 'leave' => 'institutions#leave'

      get 'teachers' => 'institutions#teachers'
      get 'roles' => 'institutions#roles', path: 'user-roles'
    end

  end

  resources :subjects do
    resources :students
      member do
        get 'members' => 'subjects#members'
      end

      resources :announcements
      resources :posts, path: 'discussions' do
      resources :comments, only: [:create, :destroy]
    end
    resources :tasks
    resources :task_types, path: 'task-types'
    # resources :past_papers, path: 'past-papers'

  end

  resources :past_papers, path: 'past-papers'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
