module UsersHelper
  def get_institution_id(user)
    user.profile.try(:institution_id)
  end

  def nav_tab_active(actions)
    if actions.include?(action_name)
      "active"
    else
      ""
    end
  end

  def determine_institution_type(user)
    case user.role
    when "student"
      :school
    else
      :school
    end
  end
end
