module AttachmentsHelper
  def attachment_icon(attachment)
    type = attachment.content_type
    case type
    when 'json'
      'fa fa-file-pdf-o'
    when 'zip'
      'fa fa-file-zip-o'
    else
      'fa fa-file-o'
    end
  end
end
