module InstitutionsHelper
  def format_institution(institution_name, year)
    unless !institution_name
      return "#{institution_name} #{format_year(year)}"
    end
    ""
  end

  def format_year(year)
    if year
      moded_year = year.to_i % 1000
      if moded_year < 10
        year_string = "0#{moded_year}"
      else
        year_string = moded_year.to_s
      end
      return "'#{year_string}" if year
    end
    ""
  end
end
