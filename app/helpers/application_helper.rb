module ApplicationHelper
  def checked_box(state)
  end

  def updated?(model) 
    model.created_at != model.updated_at
  end

  def format_string(string)
    simple_format(string)
  end

  def format_time(time)
    time.strftime("%e %b '%y at %H:%M")
  end

  def print_controller_name
    name = controller_name
    formatted_name = ''
    case name
    when 'users'
      formatted_name = name.singularize
    when 'posts'
      formatted_name = 'discussions'
    when 'past_papers'
      formatted_name = 'past papers'
    when 'task_types'
      formatted_name = 'task types'
    else
      formatted_name = name
    end

    formatted_name.capitalize
  end

  def get_manageable_object(object)
    if object.is_a?(Array)
      object.last
    else
      object
    end
  end

  def get_username(user)
    return trunc(user.username, 20) if user.username.present?
    return user.id
  end

  def get_fullname(user)
    return user.profile.fullname if user.profile.fullname
    "No Name"
  end

  def trunc(word, desired_length = 18)
    truncate(word, length: desired_length)
  end
  
  def edit_url(manageable)
    url_for(_option_to_array(:edit, manageable))
  end

  def delete_url(manageable)
    url_for(manageable)
  end

  def _option_to_array(option_to_push, manageable)
    new_manageable = manageable.clone
    if new_manageable.is_a?(Array)
      new_manageable.unshift(option_to_push)
    else
      [option_to_push, manageable]
    end
  end
end
