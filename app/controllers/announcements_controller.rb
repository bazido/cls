class AnnouncementsController < ApplicationController
  before_action :load_announcement, except: [:new, :create, :index]
  before_action :load_subject
  load_and_authorize_resource through: :subject
  respond_to :html
  def new
    @announcement = Announcement.new
  end

  def create
    @announcement = Announcement.new(announcement_params)

    @announcement.user_id = current_user.id
    @announcement.subject_id = @subject.id

    if @announcement.save
      flash[:notice] = []
      flash[:notice] << "Announcement has been posted"
    end

    respond_with(@subject, @announcement)
  end

  def edit
  end

  def update
    @announcement.assign_attributes(announcement_params)
    if @announcement.save
      flash[:notice] = []
      flash[:notice] << "Announcement has been updated successfully"
    end

    respond_with(@subject, @announcement)
  end

  def show
  end

  def index
    @announcements = Announcement.where(subject_id: @subject.id).order(updated_at: :DESC)
  end

  def destroy
    if @announcement.destroy
      flash[:notice] = []
      flash[:notice] << "The announcement has been deleted successfully"
    end

    respond_with(@subject, @announcement)
  end
  
  protected
    def announcement_params
      params.require(:announcement).permit(:title, :body)
    end

    def load_announcement
      @announcement = Announcement.find(params[:id])
    end

    def load_subject
      @subject = Subject.find(params[:subject_id])
    end
end
