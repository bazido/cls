class UsersController < ApplicationController
  before_action :load_user, except: [:new, :create, :index, :login, :sessions, :logout]
  before_action :redirect_away, except: [:index, :show, :logout, :edit, :reset_password, :update]
  load_and_authorize_resource

  layout 'single_column'
  respond_to :html, :json

  def reset_password
    if params[:new_password] && params[:new_password_confirmation]
      if params[:new_password] != params[:new_password_confirmation]
        flash[:password_errors] = "Passwords do not match"
      else
        @user.password = params[:new_password]
        @user.password_confirmation = params[:new_password_confirmation]
        @user.save
        flash[:password_notice] = "Password has been changed successfully"
      end
    else
      flash[:password_errors] = "You need to provide all the password fields to change it"
    end
    redirect_to edit_user_path(@user)
  end

  def show
    @posts = Post.commented_by(@user.id)
    render layout: 'application'
  end

  def edit
    render layout: 'big_sides_application'
  end

  def update
    @user.assign_attributes(user_params(@user))
    if @user.save && request.format.symbol != :json
      flash[:notice] = []
      flash[:notice] << "Profile updated successfully"
    end
    respond_with(@user)
  end

  def index
    # TODO: Order users by fullname
    # Users are in the same institution
    @users = User.includes(:profile).where(profiles: {institution_id: current_user.profile.institution_id}).references(:profiles).order('profiles.fullname ASC')
    render layout: 'application'
  end

  def new
    @user = User.new
    # render layout: 'single_column'
  end

  def login
    # @user = User.new
    @user_session = UserSession.new
    # render layout: 'single_column'
  end

  def create
    @user = User.new(user_params)
    @user.build_profile
    if @user.save
      # TODO: Send a pin that can be used to validate an account or reset a pin
      flash[:notice] = []
      flash[:notice] << "Your account has been created successfully"
      flash[:notice] << "Enter the pin we sent you to verify your account"
    end
    if @user.errors.any?
      render action: :new
    else
      redirect_to @user
    end
  end

  def sessions
    @user_session = UserSession.new(user_params)
    if @user_session.save
      flash[:notice] = []
      flash[:notice] << "You have been logged in successfully"
      redirect_to :root
    else
      render action: :login
    end
  end

  def logout
    @user_session = UserSession.find
    if @user_session.try(:destroy)
      flash[:notice] = []
      flash[:notice] << "You are now logged out"
    end
    redirect_to :root
  end

  protected
    def user_params(user = nil)
      if action_name == 'create'
        params.permit(:mobile_number, :password, :password_confirmation)
      elsif action_name == 'sessions'
        params.permit(:mobile_number, :password, :remember_me)
      elsif action_name == 'update'

        attribs = [:username, :email, profile_attributes: [:fullname, :bio, :institution_id, :year, :is_public, :image]]
        #TODO: Check if they have permission to update the role to
        if can?(:modify_roles, user)
          attribs.prepend(:role)
        end
        
        # For now, users can't change their mobile number
        params.require(:user).permit(attribs)

      end
    end

    def load_user
      @user = User.find(params[:id])
    end

    def redirect_away
      redirect_to current_user if current_user
    end
end
