# This is a discussion for the time being
class PostsController < ApplicationController
  before_action :load_subject
  before_action :load_post, except: [:index, :new, :create]
  before_action :create_comment, except: [:index, :new, :create] # this depends on load_post's success
  load_and_authorize_resource through: :subject
  respond_to :html

  def index
    @posts = @subject.posts.order(updated_at: :DESC)
  end

  def new
    @post = Post.new(subject: @subject, post_type: Post::DISCUSSION)
  end

  def create
    @post = @subject.posts.build(user_id: current_user.id, post_type: Post::DISCUSSION)
    @post.assign_attributes(post_params)
    if @post.save
      flash[:notice] = []
      flash[:notice] << "Discussion created successfully"
    end
    respond_with(@subject, @post)
  end

  def edit
  end

  def update
    @post.assign_attributes(post_params)
    if @post.save
      flash[:notice] = []
      flash[:notice] << "Discussion updated successfully"
    end
    respond_with(@subject, @post)
  end

  def show
  end

  def destroy
    if @post.destroy
      flash[:notice] = []
      flash[:notice] << "Discussion deleted successfully"
    end
    respond_with(@subject, @post)
  end

  protected
    def post_params
      # TODO: Add open option only if their permitted to close posts
      params.require(:post).permit(:title, :body)

    end

    def load_post
      @post = Post.find(params[:id])
    end

    def create_comment
      @comment = Comment.new(commentable_id: @post.id, commentable_type: Post.name)
    end

    def load_subject
      @subject = Subject.find(params[:subject_id])
    end
end
