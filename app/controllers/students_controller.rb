class StudentsController < ApplicationController
  before_action :load_subject
  before_action :load_student
  load_and_authorize_resource through: :subject
  respond_to :json

  def update
    @student.update_attributes(student_params)
    respond_with(@subject, @student)
  end

  protected

    def student_params
      params.require(:student).permit(:accepted)
    end

    def load_subject
      @subject = Subject.find(params[:subject_id])
    end

    def load_student
      @student = Student.find(params[:id])
    end
end
