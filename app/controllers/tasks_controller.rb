class TasksController < ApplicationController
  before_action :load_subject
  before_action :load_task, except: [:new, :create, :index]
  load_and_authorize_resource

  respond_to :html
  
  def index
    @tasks = Task.where(subject_id: @subject.id).order(:start_at, :updated_at, :created_at)
    if params[:task_type_id]
      task_type_id = params[:task_type_id].to_i.abs
      if task_type_id > 0
        @tasks = @tasks.where(task_type_id: task_type_id)
      end
    end
    @tasks = @tasks.reverse
  end

  def show
  end

  def edit
  end

  def update
    @task.assign_attributes(task_params)
    if @task.save
      flash[:notice] = []
      flash[:notice] << "Task has been updated successfully"
    end

    respond_with(@subject, @task)
  end

  def new
    @task = Task.new
    @task.start_at = 1.day.from_now.at_midday
    @task.end_at = 7.days.from_now.at_midnight
    @task.attachments.build
    if Task.types(@subject.id).length == 0
      flash.now[:alert] = []
      flash.now[:alert] << "You have to create a task type first before posting a task"
    end
  end

  def create
    @task = Task.new(task_params)

    @task.user_id = current_user.id
    @task.subject_id = @subject.id

    if @task.save
      flash[:notice] = []
      flash[:notice] << "Task has been added successfully"
    end

    respond_with(@subject, @task)
  end

  def destroy
    if @task.destroy
      flash[:notice] = []
      flash[:notice] << "Task has been deleted successfully"
    end
    respond_with(@subject, @task)
  end

  protected
    def task_params
      params.require(:task).permit(:title, :lifetime, :task_type_id, :start_at, :end_at, :body, attachments_attributes: [:id, :_destroy, :file])
    end

    def load_task
      @task = Task.find(params[:id])
    end

    def load_subject
      @subject = Subject.find(params[:subject_id])
    end
end
