class CommentsController < ApplicationController
  before_action :load_subject
  before_action :load_post
  load_and_authorize_resource through: :post

  respond_to :html
  def create
    @comment = @post.comments.build(user_id: current_user.id)
    @comment.assign_attributes(comment_params)
    if @comment.save
      flash[:notice] = []
      flash[:notice] << "Comment was created successfully"
    end
    respond_with(@subject, @post, anchor: "comment-#{@comment.id}")
  end

  def destroy
    @comment = @post.comments.find(params[:id])
    if @comment.destroy
      flash[:notice] = []
      flash[:notice] << "Comment deleted successfully"
    end
    respond_with(@subject, @post)
  end

  protected
    def comment_params
      params.require(:comment).permit(:body)
    end

    def load_subject
      @subject = Subject.find(params[:subject_id])
    end

    # Using extreme programming to just make it work
    def load_post
      @post = Post.find(params[:post_id])
    end
end
