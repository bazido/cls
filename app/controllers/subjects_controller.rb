class SubjectsController < ApplicationController
  before_action :load_subject, except: [:new, :create, :index, :list]
  respond_to :html
  load_and_authorize_resource

  def index
    if current_user.profile.institution
      @subjects = current_user.subjects.order(:name)
    else
      flash[:alert] = []
      flash[:alert] << "Please join your school on the list before going to the subjects"
      redirect_to institutions_path
    end
  end

  def new 
    @subject = Subject.new 
  end

  def create
    @subject = Subject.new(subject_params)
    @subject.user_id = current_user.id
    @subject.institution_id = current_user.profile.institution_id

    if @subject.save
      flash[:notice] = []
      flash[:notice] << "#{@subject.name} was created successfully"
    end
    
    if @subject.persisted?
      redirect_to list_institution_path(@subject.institution)
    else
      respond_with(@subject)
    end
  end

  def edit
  end

  def update
    @subject.assign_attributes(subject_params)
    if @subject.save
      flash[:notice] = []
      flash[:notice] << "#{@subject.name} was been updated successfully"
    end

    respond_with(@subject)
  end

  def members
    @members = Student.for_subject(@subject.id)
  end

  def show
    @announcements = @subject.announcements.limit(5).order(updated_at: :DESC)
  end

  def destroy
    subject_name = @subject.name
    delete_operation = false
    if @subject.destroy
      delete_operation = true
      flash[:notice] = []
      flash[:notice] = "#{subject_name} was created successfully"
    end
    if !delete_operation
      respond_with(@subject)
    else
      redirect_to @subject.institution
    end
  end

  def add
    @student = Student.where(subject_id: @subject.id, user_id: current_user.id).first
    if @student.nil?
      @student = Student.new(subject_id: @subject.id, user_id: current_user.id, accepted:  false)
      flash[:notice] = []
      if @student.save
        flash[:notice] << "The subject has been added successfully"
      else
        flash[:alert] << "System error: Could not add the subject"
      end
    else
      flash[:alert] = []
      flash[:alert] << "You have already added this subject"
    end

    redirect_back
  end

  def remove
    @student = Student.where(subject_id: @subject.id, user_id: current_user.id).first

    if !@student.nil?
      if @student.destroy
        flash[:notice] = []
        flash[:notice] << "The subject has been removed successfuly"
      else
        flash[:alert] = []
        flash[:alert] << "Server error: Could not remove the subject"
      end
    else
      flash[:alert] = []
      flash[:alert] << "You can not remove a subect you have not added"
    end

    redirect_back
  end

  protected
    def subject_params
      params.require(:subject).permit(:name, :year, :subject_type, :other_subject_type)
    end

    def load_subject
      @subject = Subject.find(params[:id].to_i)
    end

end
