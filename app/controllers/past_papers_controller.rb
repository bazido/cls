class PastPapersController < ApplicationController
  before_action :load_past_paper, except: [:index, :new, :create]
  respond_to :html
  load_and_authorize_resource

  def index
    @past_papers = PastPaper.all.order(year: :ASC, province: :ASC)
    if params[:subject_type]
      # raise params
      @past_papers = @past_papers.where('lower(subject_type) = ?', params[:subject_type].to_s.downcase)
    end
  end

  def edit
  end

  def update
    @past_paper.assign_attributes(past_paper_params)
    if @past_paper.save
      flash[:notice] = []
      flash[:notice] = "Past paper has been updated successfully"
    end

    respond_with(@past_paper) end 
  
  def new
    @past_paper = PastPaper.new
  end

  def create
    @past_paper = PastPaper.new(past_paper_params)
    @past_paper.user_id = current_user.id
    if @past_paper.save
      flash[:notice] = []
      flash[:notice] << "Past paper has been uploaded successfully"
    end
    respond_with(@past_paper)
  end

  def show
  end

  def destroy
    if @past_paper.destroy
      flash[:notice] = []
      flash[:notice] << "The past paper has been deleted successfully"
    end

    respond_with(@past_paper)
  end

  protected
    def past_paper_params
      params.require(:past_paper).permit(:producer, :paper, :province, :month, :year, :subject_type, :tags, attachments_attributes: [:id, :_destroy, :file])
    end

    def load_past_paper
      @past_paper = PastPaper.find(params[:id])
    end
end
