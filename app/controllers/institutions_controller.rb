class InstitutionsController < ApplicationController
  before_action :load_institution, except: [:new, :create, :index]
  respond_to :html
  load_and_authorize_resource

  def list
    @subjects = Subject.where(institution_id: @institution.id).order(:name)
  end

  def roles
    @allowed_roles = ['student', 'teacher', 'hr', 'admin', 'tutor']
    role = params[:role] || 'student'

    if @allowed_roles.include?(role)
      if role == 'hr'
        @role_name = 'Human resources'
      else
        @role_name = role.pluralize.capitalize
      end
    else
      role = ''
      @role_name = 'All'
    end

    @users = @users = User.joins(:profile)
    if role != ''
      @users = @users.where(role: role)
    end
    @users = @users.where(profiles: {institution_id: @institution.id}).references(:profiles).order('profiles.fullname ASC, username ASC')
  end

  def index
    @institutions = Institution.all
  end

  def show
  end

  def new
    @institution = Institution.new
  end

  def create
    @institution = Institution.new(institution_params)
    @institution.user_id = current_user.id
    if @institution.save
      flash[:notice] = []
      flash[:notice] << "Institution created successfully"

      # Make this user join the course
      current_user.profile.institution_id = @institution.id
      if !current_user.save
        flash[:alert] = []
        flash[:alert] << "System error: Unable to add you the the institution. Join it manually"
      end
      # redirect_to @institution
      # return
    end

    respond_with(@institution)
  end

  def edit
  end

  def update
    @institution.assign_attributes(institution_params)
    if @institution.save
      flash[:notice] = []
      flash[:notice] << "Institution has been updated"
    end
    respond_with(@institution)
  end

  def destroy
    # TODO: It's important to think about what must happen to everyone who has joined this institution
    if @institution.destroy
      flash[:notice] = []
      flash[:notice] = "Institution was deleted successfully"
    end
    respond_with(@institution)
  end

  def join

    if current_user.profile.institution_id == @institution.id
      flash[:alert] = []
      flash[:alert] << "You are already part of this institution"
    else
      current_user.profile.institution_id = @institution.id
      if current_user.save
        flash[:notice] = []
        flash[:notice] << "You have joined #{@institution.name}"
      end
    end

    redirect_back
  end

  def leave

    if current_user.profile.institution_id == @institution.id
      current_user.profile.institution_id = nil
      if current_user.save
        flash[:notice] = []
        flash[:notice] << "You have left #{@institution.name}"
      end
    else
      flash[:alert] = []
      flash[:alert] << "You are not part of this institution"
    end

    redirect_back
  end

  protected
    def institution_params
      params.require(:institution).permit(:name, :location, :website, :bio, :institution_type)
    end

    def load_institution
      @institution = Institution.find(params[:id])
    end
end
