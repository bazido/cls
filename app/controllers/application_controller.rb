class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user

  before_action :force_username_and_fullname, if: :not_user_controller_actions

  rescue_from CanCan::AccessDenied do |exception|
    render layout: 'application', file: '/shared/access_denied'
  end

  protected
    def force_username_and_fullname
      # raise current_user
      if current_user && (!current_user.username.try('present?') || !current_user.profile.fullname.try('present?'))
        flash.now[:notice] = flash.now[:notice] || []
        flash[:notice] = "Please choose a username and enter your fullname before continuing"
        redirect_to [:edit, current_user]
      end
    end

    def not_user_controller_actions
      controller_name != 'users' || !['new', 'create','edit', 'update', 'logout', 'sessions'].include?(action_name)
    end

    def redirect_back
      begin
        redirect_to :back
      rescue ActionController::RedirectBackError
        redirect_to :root
      end
    end

    def current_user
      return @current_user if defined?(@current_user)
      @current_user = current_user_session && current_user_session.record
    end

  private
    def current_user_session
      return @current_user_session if defined?(@current_user_session)
      @current_user_session = UserSession.find
    end
end
