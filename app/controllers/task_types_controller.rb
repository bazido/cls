class TaskTypesController < ApplicationController
  before_action :load_task_type, only: [:update]
  before_action :load_subject
  load_and_authorize_resource through: :subject

  respond_to :html, :json

  def create
    @task_type = TaskType.new(task_type_params)

    @task_type.user_id = current_user.id
    @task_type.subject_id = @subject.id

    if @task_type.save
      flash[:notice] = []
      flash[:notice] << "Task type has been added"
      redirect_to subject_task_types_path(@subject)
      return
    end

    @task_types = TaskType.where(subject_id: @subject.id).order(:name)
    render action: :new
  end

  def index
    @task_type = TaskType.new
    @task_types = TaskType.where(subject_id: @subject.id).order(:name)
  end

  def update
    @task_type.assign_attributes(task_type_params)
    @task_type.save
    respond_with(@subject, @task_type)
  end

  protected
    def load_task_type
      @task_type = TaskType.find(params[:id])
    end

    def task_type_params
      params.require(:task_type).permit(:name)
    end

    def load_subject
      @subject = Subject.find(params[:subject_id])
    end
end
