class Attachment < ActiveRecord::Base
  belongs_to :attachable, polymorphic: true

  ALLOWED_TYPES = {
    'application/pdf': 'json',
    'application/zip': 'zip'
  }

  has_attached_file :file

  validates_attachment_content_type :file, :content_type => /^application\/(zip|pdf)/
  validates_attachment_size :file, :less_than => 25.megabytes

  before_post_process :check_file_size

  def content_type
    ALLOWED_TYPES[self.file_content_type.try(:to_sym)]
  end


  private
    def check_file_size
      valid?
      errors[:file_file_size].blank?
    end

end
