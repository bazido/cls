class Profile < ActiveRecord::Base
  belongs_to :user, touch: true
  belongs_to :institution

  # image_options = {default_url: '/displays/small/missing.png', :styles => {:small => ['64x80>', :jpg], :medium => ['256x320>', :jpg]}, :convert_options => { :image_picture => '-quality 95 -strip', :icon => '-strip'}}
  image_options = {default_url: '/displays/small/missing.png', :styles => {:small => ['96x96!', :jpg], :medium => ['320x320!', :jpg]}, :convert_options => { :image_picture => '-quality 95 -strip', :icon => '-strip'}}

  unless Rails.env.development? || Rails.env.test?
    image_options.merge! :storage        => :s3,
      :s3_credentials => {
        access_key_id: ENV['S3_ACCESS_KEY_ID'],
        secret_access_key: ENV['S3_SECRET_ACCESS_KEY']
      },
      :path           => ':attachment/:id/:style.:extension',
      :bucket         => ENV['S3_USER_BUCKET']
  end  

  has_attached_file :image, image_options
  validates_attachment_content_type :image, :content_type => /^image\/(png|gif|jpeg)/
  validates_attachment_size :image, :less_than => 2.megabytes

  before_post_process :check_file_size

  private
    def check_file_size
      valid?
      errors[:image_file_size].blank?
    end

end
