class Student < ActiveRecord::Base
  belongs_to :user
  belongs_to :subject
  scope :for_subject, lambda { |subject_id| joins(:user).where(subject_id: subject_id).merge(User.joins(:profile).order('profiles.fullname ASC, username ASC').references(:profiles))}
end
