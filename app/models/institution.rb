class Institution < ActiveRecord::Base
  belongs_to :user

  @@types = ['school', 'varsity', 'online']

  validates_presence_of :name
  validates_presence_of :location
  validates_presence_of :bio
  validates_inclusion_of :institution_type, in: @@types

  before_validation :prepare_data

  def self.types
    return @@types.collect do |type|
      [type.capitalize, type]
    end
  end

  def self.all_for_select(type)

    case type
    when :school
      institutions = self.where(institution_type: 'school')
    else
      institutions = self.all
    end

    return institutions.collect do |institution|
      [institution.name, institution.id]
    end

  end

  protected
    def prepare_data
      self.location = self.location.to_s.strip
      self.name = self.name.to_s.strip
      self.bio = self.bio.to_s.strip
    end
end
