class Ability
  include CanCan::Ability 
  def initialize(user)

    user ||= User.new(role: 'guest') # Default is student, prevent

    # raise user
    if !user.persisted?
      can [:login, :new, :sessions, :create], User
    end

    if user.is_student? || user.is_tutor? || user.is_teacher? || user.is_hr?

      # User
      can [:edit, :update, :reset_password], User do |u|
        u.id == user.id
      end
      can [:show, :index, :list, :logout], User

      # Institution can [:show, :index], [Institution, Subject, PastPaper, Post, Announcement, Task]

      # Subject
      can [:list, :roles, :join, :leave], Institution
      can [:members, :show, :index], Subject
      can [:add, :remove], Subject do |subject| 
        # This actually adds a student
        subject.institution.id == subject.institution.id
      end
      can [:new, :create], Post do |post|
        post.subject.students.where(user_id: user.id).first.try(:accepted)
      end
      can [:edit, :update, :destroy], Post do |post|
        post.subject.students.where(user_id: user.id).first.try(:accepted) && post.user.id == user.id
      end
      can [:show, :index], [Announcement, Post, Task] do |manageable|
        manageable.subject.institution.id == user.profile.institution.id
      end
      can [:show, :index], [Institution, PastPaper]

    end

    if user.is_student? || user.is_tutor?
      can [:create, :destroy], Comment do |comment|
        accepted_student = comment.commentable.subject.students.where(user_id: user.id).first.try(:accepted)
        if comment.persisted?
          accepted_student && comment.user.id == user.id
        else
          accepted_student
        end
      end
    end

    if user.is_student?
    end

    if user.is_tutor?
    end

    if user.is_teacher? || user.is_hr?
      can [:edit, :update], Student
      can [:create, :destroy], Comment
    end

    if user.is_teacher?
      can [:modify_roles, :edit, :update], User do |u|
        User.role_priority(user.role) > User.role_priority(u.role)
      end
      can [:show, :index, :new, :create, :edit, :update], Subject do |subject|
        if subject.persisted?
          subject.user_id == user.id
        else
          true
        end
      end
      can [:manage], [Announcement, Post, Task, TaskType] do |manageable|
        manageable.subject.institution.id == user.profile.institution.id
      end
    end

    if user.is_hr?
      can :manage, Institution do |institution|
        if institution.persisted?
          institution.id == user.profile.institution.id
        else
          true
        end
      end
      can :manage, Subject do |subject|
        subject.institution.id == user.profile.institution.id
      end
      can [:modify_roles, :edit, :update], User do |u|
        User.role_priority(user.role) >= User.role_priority(u.role) && u.id != user.id
      end
    end

    if user.is_admin?
      can :manage, :all
      cannot [:modify_roles], User do |u|
        u.id == user.id
      end
    end

  end

end
