class User < ActiveRecord::Base
  ROLES = {
    'student': 1,
    'tutor': 2,
    'teacher': 3,
    'hr': 4,
    'admin': 5
  }

  has_one :profile, dependent: :destroy
  has_many :students
  has_many :subjects, through: :students # This makes it hard to query for where the student is a teacher
  has_many :posts, dependent: :destroy

  before_validation :username_can_not_be_a_number
  validates_length_of :username, maximum: 20
  validates_uniqueness_of :mobile_number, message: "is already taken, do a password reset if you own the number"
  validates_length_of :mobile_number, within: (10..12)
  validates_format_of :password, with: /[^\d]+/, message: "is not a pin, it can not be only numbers", if: :password_is_pin

  acts_as_authentic do |c|
    c.login_field = 'mobile_number'
    c.validate_email_field = false
    c.validate_login_field = false
    c.merge_validates_length_of_password_field_options(minimum: 8) 
    c.merge_validates_length_of_password_confirmation_field_options(allow_blank: true) 
  end 

  accepts_nested_attributes_for :profile, update_only: true 
  before_validation :prepare_data

  def confirmed?
    # TODO: Change confirmation
    true
  end

  def self.role_priority(role)
    ROLES[role.to_sym]
  end

  def modifiable_roles(kv_pairs = true)
   
    case self.role
    when 'admin'
      roles = ['admin', 'hr', 'student', 'teacher', 'tutor']
    when 'hr'
      roles = ['hr', 'student', 'teacher', 'tutor']
    when 'student'
      roles = ['student']
    when 'teacher'
      roles = ['student', 'teacher', 'tutor']
    when 'tutor'
      roles = ['student', 'tutor']
    else
      roles = ['student']
    end

    if kv_pairs
      roles.collect do |role|
        [role, role]
      end
    else
      roles
    end
  end

  def is_student?
    self.role == 'student'
  end

  def is_tutor?
    self.role == 'tutor'
  end

  def is_teacher?
    self.role == 'teacher'
  end

  def is_hr?
    self.role == 'hr'
  end

  def is_admin?
    self.role == 'admin'
  end

  protected
    def prepare_data

      self.mobile_number = extract_only_digits(self.mobile_number)

    end

    def extract_only_digits(text)
      digits = text.to_s.gsub(/[^\d]/, '')
      digits
    end

    def password_is_pin
      is_zero = self.password =~ /\A[\d]+\z/

      # zero -> true, nil -> false
      return is_zero == 0
    end

    def username_can_not_be_a_number
      if (self.username =~ /\A[\d]+\z/) == 0
        self.errors.add(:username, 'can not be a number')
      end
    end

end
