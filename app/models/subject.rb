class Subject < ActiveRecord::Base
  YEAR_OF_CREATION = 2016
  belongs_to :user
  belongs_to :institution
  has_many :students
  has_many :users, through: :students, dependent: :destroy
  has_many :announcements, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :tasks, dependent: :destroy
  has_many :task_types, dependent: :destroy

  validates_presence_of :name
  validates_presence_of :year
  validates_numericality_of :year, greater_than_or_equal_to: YEAR_OF_CREATION
  validates_presence_of :subject_type

  before_validation :prepare_data

  protected
    def prepare_data
      self.name = self.name.to_s.strip
      self.subject_type = self.subject_type.to_s.strip

      self.year = self.year.to_i.abs
      # self.year = YEAR_OF_CREATION if self.year < YEAR_OF_CREATION
    end
end
