class PastPaper < ActiveRecord::Base
  @@provinces = [
    'Eastern Cape',
    'Free State', 
    'Gauteng', 
    'Kwa Zulu Natal',
    'Limpopo', 
    'Mpumalanga', 
    'Northen Cape',
    'North West',
    'Western Cape',
  ]

  @@months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ]

  belongs_to :subject
  belongs_to :user

  has_many :attachables
  has_many :attachments, as: :attachable, dependent: :destroy
  accepts_nested_attributes_for :attachments, update_only: true, allow_destroy: true, reject_if: :all_blank

  def self.provinces
    @@provinces.collect do |province|
      [province, province]
    end
  end

  def self.months
    @@months.collect do |month|
      [month, month.parameterize]
    end
  end
end
