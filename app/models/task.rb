class Task < ActiveRecord::Base
  belongs_to :subject
  belongs_to :user
  belongs_to :task_type
  has_many :attachables
  has_many :attachments, as: :attachable, dependent: :destroy
  accepts_nested_attributes_for :attachments, update_only: true, allow_destroy: true, reject_if: :all_blank

  before_validation :prepare_data

  def self.types(subject_id)
    types = TaskType.where(subject_id: subject_id)
    types.collect do |type|
      [type.name, type.id]
    end
  end

  def self.lifetimes
    [['Unlimited', 'unlimited'], ['Range', 'range']]
  end

  def closes?
    self.lifetime == 'range'
  end

  protected
    def prepare_data
      self.body = self.body.to_s.strip.gsub(/([\s]){3,}/, "\n\n")
    end

end
