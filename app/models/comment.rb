class Comment < ActiveRecord::Base
  belongs_to :commentable, polymorphic: true, touch: true
  belongs_to :user

  before_validation :prepare_data

  protected
    def prepare_data
      self.body = self.body.to_s.strip.gsub(/[\n]{2,}/, "\n")
    end
end
