class Post < ActiveRecord::Base
  belongs_to :subject
  belongs_to :user
  DISCUSSION = 'discussion'

  has_many :commentables
  has_many :comments, as: :commentable, dependent: :destroy

  validates_length_of :title, within: (10..80)
  validates_length_of :body, minimum: 10

  before_validation :prepare_data

  scope :commented_by, lambda {|user_id| Post.joins(:comments).where(comments: {user_id: user_id}).distinct(id: true).references(:comments)}

  protected
    def prepare_data
      self.title = self.title.to_s.strip
      self.body = self.body.to_s.strip.gsub(/([\s]){3,}/, "\n\n")

    end
end
