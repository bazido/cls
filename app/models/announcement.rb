class Announcement < ActiveRecord::Base
  belongs_to :subject
  belongs_to :user
  validates_presence_of :title
  validates_length_of :title, within: (10..70), if: lambda {|current_object| current_object.title.length > 0}
  validates_length_of :body, minimum: 10, if: lambda {|current_object| current_object.body.length > 0}
  validates_presence_of :body

  before_validation :prepare_data

  protected
    def prepare_data
      self.title = self.title.to_s.strip
      self.body = self.body.to_s.strip.gsub(/([\s]){3,}/, "\n\n")
    end
end
