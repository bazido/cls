class UserSession < Authlogic::Session::Base
  before_validation :extract_mobile_number_digits
  generalize_credentials_error_messages true

  protected
    def extract_mobile_number_digits
      self.mobile_number = extract_digits(self.mobile_number) if self.mobile_number != nil
    end

    def extract_digits(text)
      return text.to_s.gsub(/[^\d]/, '')
    end
end
